/*JavaScript має 8 основних типів даних:

  - number - будь-які числа у діапазоні ±2^53, у тому числі десяткові дроби;

  - bigint - для цілих чисел довільної довжини;

  - string - для рядків;

  - boolean - для true/false;

  - null - для невідомих значень;

  - undefined для неприсвоєних значень;

  - object - для більш складних структур даних;

  - symbol - для унікальних ідентифікаторів.*/


/* == це звичайний оператор порівняння, але він не відрізняє 0 від false;

=== це оператор строгої рівності, він перевіряє рівність без перетворення типів.*/


// Оператор - це внутрішня функція JavaScript, яка виконує якісь конкретні дії і повертає результат.


let userName = prompt('Enter your name');
let userAge = Number(prompt('Enter your age'));

while (!userName || (!userAge || isNaN(userAge))) {
    if (!userName) {
        userName = prompt('Enter your name again');
    }
    if (!userAge || isNaN(userAge)) {
        userAge = Number(prompt('Enter your age again'));
    }    
};

if (userAge < 18) {
    alert('You are not allowed to visit this website');
}
if (userAge >= 18 && userAge <= 22) {
    let toAccept = confirm('Are you sure you want to continue');
    
    if (toAccept) {
        alert(`Welcome, ${userName}`);
    } else {
        alert('You are not allowed to visit this website')
    }
}
if (userAge > 22) {
    alert(`Welcome, ${userName}`);
};